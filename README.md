# Modele épidemique

Programme permettant de simuler une épidémie à l'aide des modèles SIR, SEIR et SEIR Naissance. 

*Réalisé par ABDELGHANI Tarek et BAUVENT Kilian*


## Recupérer le projet

Pour récupérer le projet il suffit de clone le projet sur votre machine avec la commande:

```shell
git clone https://forge.univ-lyon1.fr/p1803623/modele-epidemique.git
```


## Comment sa marche ?

### Programme 

Pour faire fonctionner la simulation il faut d'abord que vous remplissiez un fichier texte comme le fichier "data.txt" donné dans le projet.
Dans ce fichier texte vous renseignez tout les paramètres de la simulation.
* le nombre de personnes de base Saine, infecté, retirer ou/et exposé 
* les coefficients des modèles utilisés 
* la taille du monde sur lequels les différente personnes vont se déplacer. Vous pouvez ainsi choisir de faire une simulation sans spatialisation en mettant M=1 et N=1 ou avec spatialisation en donnant n'importe quelle autre taille de monde. 
* Les politiques publiques que vous appliquez durant la simulation en mettant 'true' ou 'false'
* Les paramètres des politiques publiques 

Une fois votre fichier rempli, placer le au même endroit que 'data.txt'. 

Vous pouvez maintenant compiler et lancer le programme en vous plaçant dans le dossier modele-epidemique et en faisant : 

```shell
ant run -Dfile='nomDuFichier.txt'
```

Vous pouvez clean en faisant :

```shell
ant clean
```

### Documentation 

Toutes les fonctions sont documentées pour voir la documentation il vous suffit de générer la javadoc avec la commande.

```shell
ant doc
```

Il vous restera plus qu'a lancer sur votre navigateur le index.html présent dans le dossier doc. 

## Fonctionnement du programme

Une fois le programme lancer une interface vous demandera avec quel modèle parmi le modèle SIR, SEIR Normal, SEIR Naissance vous voulez simuler

**IMPORTANT** 
* Assurez-vous bien que tous les paramètres nécessaires pour la simulation choisie sont bien remplis. Les paramètres necessaires pour chaque modèle sont stipulé dans la partie 'Fonctionnalité' de ce readme

* Le fichier texte doit absolument garder la même forme que le fichier 'data.txt' même si tout les paramètres ne sont pas utilisés

Il vous demandra combien de pas vous voulez que la simulation tourne 
Une fois cela réalisé il n'y a plus rien à faire.
Une fois la simulation terminée le programme vous affiche les résultats sous forme de tableau. 

## Fonctionnalité

Voici toutes les Fonctionnalités du programme ainsi que la manière de les utiliser:

### SIR
Le modèle sir est un modèle basé sur les 3 etats Sains, Infectée, Retirée 

*Les formules exactes sont disponible dans les liens* 


Les paramètres necessaires à remplir dans le fichier txt pour ce modèle sont les suivants :
* S: nombre de personnes Saintes au commencement (entier > 0)
* I: nombre de personnes Infecter au commencement (entier > 0)
* R: nombre de personnes Retirer au commencement (entier > 0)

* Beta: coefficient de chance d'être infecté (reelle entre 0 et 1)
* Gamma: probabilité d'être retiré en étant infectée (reelle entre 0 et 1)

* M: taille horizontal (entier > 0)
* N: taille verticale (entier > 0)
* Si M et N valent 1 il n'y aura pas de spatialisation dans le cas contraire si*

* confinement, portDuMasque, quarantaine, vaccination: application ou non des politiques publiques (true ou false)

### SEIR Normal
Le modèle seir normal est un modèle similaire au modèle SIR en rajoutant la phase incubation 

*Les formules exactes sont disponible dans les liens* 

Les paramètres necessaires à remplir dans le fichier txt pour ce modèle sont les suivants :
* Les même que pour l'utilisation de SIR
* E: nombre de personnes Exposées au commencement (entier > 0)
* Alpha: Probabilité d'être infecté en étant exposée (reelle entre 0 et 1)


### SEIR Naissance
Le modèle seir naissance est un modèle similaire au modèle SEIR en rajoutant la gestion des naissances et des morts 

*Les formules exactes sont disponible dans les liens* 


Les paramètres necessaires à remplir dans le fichier txt pour ce modèle sont les suivants :
* Les même que pour l'utilisation de SEIR Normal
* Eta: Proportion de naissance (reelle entre 0 et 1)
* Mu: Proportion de mort naturelle (reelle entre 0 et 1)


### Politique Publiques

#### Confinement
Avec le confinement les personnes on plus qu'une chance sur 3 de ce déplacer. 

Les paramètres necessaires à remplir dans le fichier txt pour utilisé cette politique publique sont:
* Confinement: à mettre sur true


#### Port du masque
Les personnes dans un certain pourcentage des cases portent un masque et ont une chance d'être infectées réduite de moitié.

Les paramètres necessaires à remplir dans le fichier txt pour utilise cette politique publique sont:
* portDuMasque: à mettre sur true
* coefPortDuMasque: proportion de personnes qui portent un masque (réelle entre 0 et 1)



#### Quarantaine 
Les personnes dans un certain pourcentage des cases vont en quarantaine et ne peuvent plus être infectées

Les paramètres necessaires à remplir dans le fichier txt pour utilise cette politique publique sont:
* quarantaine: à mettre sur true
* coefQuarantaine: proportion de case qui sont en quarantaine (reelle entre 0 et 1)


#### Vaccination
Avec la vaccination un certain pourcentage des personnes saines passe en retirer 

Les paramètres necessaires à remplir dans le fichier txt pour utilise cette politique publique sont:
* vaccination: à mettre sur true
* coefVaccination: proportion de personnes qui sont vacciné par pas (reelle entre 0 et 1)


## Methodologie 

### articulation conception/codage

On a d'abord réalisé une première conception du projet avec l'aide de diagrammes puis l'on a commencé le codage en suivant cette conception. À la suite de difficulté rencontrée lors de la programmation qu'on avait pas imaginée lors de la conception on a decidé de réadapter les diagrammes initiaux en prenant en compte ces difficultés et le projet final résulte donc du codage de cette nouvelle version de la conception. 

### déroulé temporel

La réalisation de ce projet a été répartie sur trois semaines avec avec la plupart du travail concentré entre le 5 janvier et le 13 janvier 2021.

### répartition des taches

La plupart du travail a été réalisé en coopération et en communication vocal en travaillant chacun de notre  côté ou sur le même écran. 
Mais l'on peut quand même donner quelques taches qui ont été réalisé que par l'un de  nous deux. 
Tarek c'est personnellement occupé de la lecture et gestion du fichier .txt ainsi que la grande globalité 
de la doc mais aussi la mise en place des politiques publiques dans le code. Et Kilian c'est personnellement occupé de la fonctions de déplacement ainsi que de la structure 
des fonctions "calculer" et de la simulation en général.

## Liens

diagrammes : https://forge.univ-lyon1.fr/p1803623/modele-epidemique/-/tree/master/UML

explication modèle sir : http://images.math.cnrs.fr/Modelisation-d-une-epidemie-partie-1.html 

explication modèle seir : https://images.math.cnrs.fr/Modelisation-d-une-epidemie-partie-2.html


