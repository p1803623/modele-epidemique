
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe abstraite représentant les modeles de type SEIR.
 */
abstract class SEIR extends Modele{
     
    /**
    * Fonction abstraite permettant de recuperer le nombre de personnes passant d'un etat à un autre.
    * @param S entier representant le nombre de sain
    * @param E entier representant le nombre d'exposé
    * @param I entier representant le nombre d'infecté
    * @param R entier representant le nombre de retiré
    * @param x taille de l'axe horizontale
    * @param y taille de l'axe verticale
    * @return tableau des transitions ([0]: S to E, [1]: E to I, [2]: I to R)
    */
    abstract int[] getTransitions(int S, int E, int I, int R, int x, int y);
    
    
    /**
     * Constructeur de la classe.
     * Initialise les valeurs de Modele a partir d'un fichier; initialise tab et assigne de manière aléatoire des etats au personne du monde
     *@param  nomFichier nom du fichier à ouvrir pour lire les données.
     */
    public SEIR(String nomFichier){
        super(nomFichier);
        
        this.tab = new ArrayList<>(4);

        tab.add(new ArrayList <>());
        tab.add(new ArrayList <>());
        tab.add(new ArrayList <>());
        tab.add(new ArrayList <>());
        
        tab.get(0).add(S);
        tab.get(1).add(E);
        tab.get(2).add(I);
        tab.get(3).add(R);
        
        
       
        // set les etats de base
        List<String> listeEtats = new ArrayList();
        for(int i = 0; i<S; i++){
            listeEtats.add("S");
        }
        
        for(int i = 0; i<E; i++){
            listeEtats.add("E");
        }
        
        for(int i = 0; i<I; i++){
            listeEtats.add("I");
        }
        
        for(int i = 0; i<R; i++){
            listeEtats.add("R");
        }
        
        Collections.shuffle(listeEtats);
        monde.setEtats(listeEtats);

    }
    
    /**
     * Surcharge de la procedure calculer de la classe parent.
     * Calcul les prochaines valeurs de S, E, I et R 
     */
    @Override
    protected void calculer(){
        int m = monde.getM();
        int n = monde.getN();
        
        if (politique.getPortDuMasque() == true)
        {
            this.Beta = Beta*coefPortDuMasque;
        }
        
        int sommeS = 0;
        int sommeE = 0;
        int sommeI = 0;
        int sommeR = 0;

        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                List<Personne> PersonneSaine = monde.getPersonneDansCaseParEtat(i,j,"S");
                List<Personne> PersonneExpose = monde.getPersonneDansCaseParEtat(i,j,"E");
                List<Personne> PersonneInfecter = monde.getPersonneDansCaseParEtat(i,j,"I");
                List<Personne> PersonneRetirer = monde.getPersonneDansCaseParEtat(i,j,"R");
                
                int [] transition = getTransitions(PersonneSaine.size(), PersonneExpose.size(), PersonneInfecter.size(), PersonneRetirer.size(), i, j);
                
                if(transition[0] > 0)
                {
                    int indexS = transition[0];
                    if(transition[0] > PersonneSaine.size()){
                        indexS = PersonneSaine.size();
                    }
                    for(int s = 0; s< indexS ; s++){
                        PersonneSaine.get(s).setEtat("E");
                    }
                }
                
            
                if(transition[1] > 0){
                
                    int indexE = transition[1];
                    if(transition[1] > PersonneExpose.size()){
                        indexE = PersonneExpose.size();
                    }
                    for(int e = 0; e< indexE ; e++){
                        PersonneExpose.get(e).setEtat("I");
                    }
                }
                
                
                if(transition[2] > 0)
                {
                    int indexI = transition[2];
                    if(transition[2] > PersonneInfecter.size()){
                        indexI = PersonneInfecter.size();
                    }
                    for(int r = 0; r< indexI ; r++){
                        PersonneInfecter.get(r).setEtat("R");
                    }
                }
                
                // ajout des nouvelle valeur a la somme total de la grille
                sommeS += monde.getNbEtatDansCase(i,j,"S");
                sommeE += monde.getNbEtatDansCase(i,j,"E");
                sommeI += monde.getNbEtatDansCase(i,j,"I");
                sommeR += monde.getNbEtatDansCase(i,j,"R");
            }
        }
        
        
        // ajout des valeurs dans le tableau final
        tab.get(0).add(sommeS);
        tab.get(1).add(sommeE);
        tab.get(2).add(sommeI);
        tab.get(3).add(sommeR);
    }
    
    
    @Override
     public void affichageResultat(){
        System.out.println("");
        System.out.println("--- Resultats de la simulation  --- ");
        
        System.out.println("");
        System.out.println("L'évolution du nombre de Sain est : ");
        System.out.println(tab.get(0));
        
        System.out.println("");
        System.out.println("L'évolution du nombre de Exposée est : ");
        System.out.println(tab.get(1));
        
        System.out.println("");
        System.out.println("L'évolution du nombre d' Infecté est : ");
        System.out.println(tab.get(2));
        
        System.out.println("");
        System.out.println("L'évolution du nombre de Retiré est : ");
        System.out.println(tab.get(3));
    }
}
