
/**
 * Classe représentant les politiques public 
 */
public class PolitiquePublique {
    private boolean confinement, portDuMasque, quarantaine, vaccination ;
    
    
    /**
     * Constructeur de la classe. Assigne les valeurs de base de la classe.
     * @param c le confinement
     * @param p le portDuMasque
     * @param q la quarantaine
     * @param v la vaccination
     */
    public PolitiquePublique(boolean c,boolean p,boolean q,boolean v)
    {
        confinement = c;
        portDuMasque = p;
        quarantaine = q;
        vaccination = v;
    }
    
    /**
     * Recupere la variable confinement.
     * @return booleen confinement
     */
    public boolean getConfinement(){
        return confinement;
    }
    
    /**
     * Recupere la variable PortDuMasque.
     * @return booleen PortDuMasque
     */
     public boolean getPortDuMasque(){
        return portDuMasque;
    }
    
    /**
     * Recupere la variable quarantaine.
     * @return booleen quanrantaine
     */
    public boolean getQuarantaine(){
        return quarantaine;
    }
    
    /**
     * Recupere la variable vaccination.
     * @return booleen vaccination
     */
    public boolean getVaccination(){
        return vaccination;
    }
    
}
