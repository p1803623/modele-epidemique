
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Classe SIR qui traite du modele SIR.
 */
public class SIR extends Modele{
    /**
     * Constructeur de la classe.
     * Initialise les valeurs de Modele a partir d'un fichier, initialise tab et assigne de manière aléatoire des etats au personne du monde
     *@param  nomFichier nom du fichier à ouvrir pour lire les données.
     */
    public SIR(String nomFichier){
        super(nomFichier);
        
        this.tab = new ArrayList<>(3);
        
        tab.add(new ArrayList <>());
        tab.add(new ArrayList <>());
        tab.add(new ArrayList <>());
        
        tab.get(0).add(S);
        tab.get(1).add(I);
        tab.get(2).add(R);
        
        
        // set les etats de base
        List<String> listeEtats = new ArrayList();
        for(int i = 0; i<S; i++){
            listeEtats.add("S");
        }
        
        for(int i = 0; i<I; i++){
            listeEtats.add("I");
        }
        
        for(int i = 0; i<R; i++){
            listeEtats.add("R");
        }
        
        Collections.shuffle(listeEtats);
        monde.setEtats(listeEtats);
    }
    
    /**
     * Surcharge de la procedure calculer de la classe parent.
     * Calcul les prochaines valeurs S, I et R
     */
    @Override
    protected void calculer(){
        int m = monde.getM();
        int n = monde.getN();
        
        int sommeS = 0;
        int sommeI = 0;
        int sommeR = 0;
        

        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                
                // Gestion PortDuMasque
                double BetaPortDuMasque = Beta ;
                if (politique.getPortDuMasque() == true )
                {
                    int randBetween1_100 = ThreadLocalRandom.current().nextInt(1, 100);
                    if(randBetween1_100 <= coefPortDuMasque * 100)
                    {
                        BetaPortDuMasque = Beta*0.5;
                    }
                }
                
                
                //Gestion Quarantaine
                int quarantaine = 1;
                if (politique.getQuarantaine() == true)
                {
                    int randBetween1_100 = ThreadLocalRandom.current().nextInt(1, 100);
                    if(randBetween1_100 <= coefQuarantaine * 100)
                        {
                            quarantaine = 0;
                        }
                }
                
                List<Personne> PersonneSaine = monde.getPersonneDansCaseParEtat(i,j,"S");
                List<Personne> PersonneInfecter = monde.getPersonneDansCaseParEtat(i,j,"I");
            
                int StoI = getNbPersonneGoStoI(PersonneSaine.size(), PersonneInfecter.size(), BetaPortDuMasque, quarantaine);
                int ItoR = getNbPersonneGoItoR(PersonneInfecter.size());
                if(StoI > 0)
                {
                    int indexS = StoI;
                    if(StoI > PersonneSaine.size()){
                        indexS = PersonneSaine.size();
                    }
                    for(int s = 0; s< indexS ; s++)
                    {
                        PersonneSaine.get(s).setEtat("I");
                    }
                }
                
                if(ItoR > 0)
                {
                    for(int r = 0; r< ItoR ; r++)
                    {
                        PersonneInfecter.get(r).setEtat("R");
                    }
                }
                
                // ajout des nouvelle valeur a la somme total de la grille
                sommeS += monde.getNbEtatDansCase(i,j,"S");
                sommeI += monde.getNbEtatDansCase(i,j,"I");
                sommeR += monde.getNbEtatDansCase(i,j,"R");
            }
        }
        
        // ajout des valeurs dans le tableau final
            tab.get(0).add(sommeS);
            tab.get(1).add(sommeI);
            tab.get(2).add(sommeR);
       
    }
    
    
    /**
     * Retourne le nombre de personne passant de S à I.
     * @param S entier representant le nombre de sain
     * @param I entier representant le nombre d'infecté
     * @param beta proportion de personne infectée
     * @param quarantaine proportion de personne en quarantaine
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoStoI(int S, int I, double beta,int quarantaine){
        return (int) Math.round(beta*S*I*quarantaine);
    }
    
    /**
     * Retourne le nombre de personne passant de I à R.
     * @param I entier representant le nombre d'infecté
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoItoR(int I){
        return (int) Math.round(Gamma*I);

    }
    
    
    @Override
    public void affichageResultat(){
        System.out.println("");
        System.out.println("--- Resultats de la simulation  --- ");
        
        System.out.println("");
        System.out.println("L'évolution du nombre de Sain est : ");
        System.out.println(tab.get(0));
        
        System.out.println("");
        System.out.println("L'évolution du nombre d' Infecté est : ");
        System.out.println(tab.get(1));
        
        System.out.println("");
        System.out.println("L'évolution du nombre de Retiré est : ");
        System.out.println(tab.get(2));
    }
} 
