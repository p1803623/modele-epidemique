
import java.util.Scanner;




/**
 * Classe main 
 * @author ABDELGHANI Tarek et BAUVENT Kilian
 */
public class modele_epidemique {

    //lecture menu + ecriture dans fichier
    public static void main(String[] args) {
        System.out.println("--- MENU ---");
        System.out.println("Choix Type Modele : ");
        System.out.println("1- SIR");
        System.out.println("2- SEIR Naissance");
        System.out.println("3- SEIR Normal");
        
        Scanner reader = new Scanner(System.in);
        int choixModele = 0;
        String configFile = "data.txt";
        if (args.length > 0)
        {
            configFile = args[0];
        }
        
        while(choixModele < 1 || choixModele > 3){
            choixModele = reader.nextInt();
        }
        
        int t = 0;
        System.out.println("Choix Nombre de pas : ");
        while(t < 1){
            t = reader.nextInt();
        }
        reader.close();
        
        switch(choixModele){
            case 1: SIR sir = new SIR(configFile);
                    sir.simulation(t);
                    sir.affichageResultat();
                    break;
            case 2: SEIR_Nais seir_n = new SEIR_Nais(configFile);
                    seir_n.simulation(t);
                    seir_n.affichageResultat();
                    break;
            case 3: SEIR_Normal seir_normal = new SEIR_Normal(configFile);
                    seir_normal.simulation(t);
                    seir_normal.affichageResultat();
                    break;
        }
    }      
}
