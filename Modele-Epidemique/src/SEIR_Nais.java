
import java.util.concurrent.ThreadLocalRandom;


/**
 * Classe qui represente le modèle SEIR Naissance.
 */
public class SEIR_Nais extends SEIR{
     /**
     * Constructeur de la classe.
     * Appel le constructeur de SEIR
     *@param  nomFichier nom du fichier à ouvrir pour lire les données.
     */
    public SEIR_Nais(String nomFichier){
        super(nomFichier);
    }
    
    /**
    * Classe abstraite permettant de recuperer le nombre de personnes passant d'un etat à un autre et gère les morts et naissance.
    * @param s entier representant le nombre de sain
    * @param e entier representant le nombre d'exposé
    * @param i entier representant le nombre d'infecté
    * @param r entier representant le nombre de retiré
    * @param x taille de l'axe horizontale
    * @param y taille de l'axe verticale
    * @return tableau des transitions ([0]: S to E, [1]: E to I, [2]: I to R)
    */
    @Override
    protected int[] getTransitions(int s, int e, int i, int r, int x, int y){
        
        //Gestion PortDuMasque
        double BetaPortDuMasque = Beta ;
        if (politique.getPortDuMasque() == true )
        {
            int randBetween1_100 = ThreadLocalRandom.current().nextInt(1, 100);
            if(randBetween1_100 <= coefPortDuMasque * 100)
            {
                BetaPortDuMasque = Beta*0.5;
            }
        }
        
        //Gestion Quarantaine
        int quarantaine = 1;
        if (politique.getQuarantaine() == true)
        {
            int randBetween1_100 = ThreadLocalRandom.current().nextInt(1, 100);
            if(randBetween1_100 <= coefQuarantaine * 100)
                {
                    quarantaine = 0;
                }
        }

        int[] transitions = new int[3];
        transitions[0] = getNbPersonneGoStoE(S, I, BetaPortDuMasque,quarantaine);
        transitions[1] = getNbPersonneGoEtoI(E);
        transitions[2] = getNbPersonneGoItoR(I);
        
        // Gestion des morts et naissance, faut faire un N to S et un ALL to decédé.
        gestionNaissanceMort(s, e, i, r, x, y);
     
        
        return transitions;
    }
    
      /**
     * Retourne le nombre de personne passant de S à E.
     * @param S entier representant le nombre de sain
     * @param I entier representant le nombre d'infecté
     * @param beta proportion de personne infectée
     * @param quarantaine proportion de personne en quarantaine
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoStoE(int S, int I,double beta,int quarantaine){
        return (int) Math.round((beta*S*I)*quarantaine); 
    }
    
    /**
     * Retourne le nombre de personne passant de E à I.
     * @param E entier representant le nombre d'exposé
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoEtoI(int E){
        return (int) Math.round(Alpha*E); 
    }
    
    /**
     * Retourne le nombre de personne passant de I à R.
     * @param I entier representant le nombre d'infecté
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoItoR(int I){
        return (int) Math.round(Gamma*I);
    }
    
     /**
     * Procedure ajoute et retire des personnes dans la case x,y en fonction des naissances et morts.
     * @param s le nombre de perosnnes saines
     * @param e le nombre de perosnnes exposées
     * @param i le nombre de perosnnes infectées
     * @param r le nombre de perosnnes retirées
     * @param x taille de l'axe horizontale
     * @param y taille de l'axe verticale
     */
    private void gestionNaissanceMort(int s, int e, int i, int r, int x, int y){
        int n = s + e + i + r;
        int naissance = getNaissance(n);
        int morts[] = getMorts(s,e,i,r);
        
        monde.removeXpersonneByEtatInCase(x,y, morts[0], "S");
        monde.removeXpersonneByEtatInCase(x,y, morts[1], "E");
        monde.removeXpersonneByEtatInCase(x,y, morts[2], "I");
        monde.removeXpersonneByEtatInCase(x,y, morts[3], "R");
        
        monde.ajouteXpersonneWithEtatInCase(x,y, naissance, "S");
        
    }
    
     /**
     * Retourne le nombre de personne qui sont nées.
     * @param N nombre de personne au total
     * @return un entier qui correspond au nombre de naissance qui s'ajouteront à l'etat S
     */
    private int getNaissance(int N){
        return (int) Math.round(Eta*N);
    }
    
    
    /**
     * Retourne le nombre de personne morte dans chaque état.
     * @param s entier representant le nombre de sain
     * @param e entier representant le nombre d'exposé
     * @param i entier representant le nombre d'infecté
     * @param r entier representant le nombre de retiré
     * @return tableau des morts par etat ([0]: "S", [1]: "E", [2]: "I", [3]: "R")
     */
    private int[] getMorts(int s, int e, int i, int r){
        int [] morts = new int[4];
        morts[0] = (int) Math.round(Mu*s);
        morts[1] = (int) Math.round(Mu*e);
        morts[2] = (int) Math.round(Mu*i);
        morts[3] = (int) Math.round(Mu*r);
        
        return morts;
    }
    
    
}
