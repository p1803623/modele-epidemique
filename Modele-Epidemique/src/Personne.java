/**
 * Classe representant une personne
 */
public class Personne {
    private int idPersonne;
    private int age;
    private String etat;
    
    /**
     * Constructeur par copie de la classe. Crée une personne avec un id, un age et un etat.
     * @param id l'id de la personne
     * @param age son age
     * @param etat son etat
     */
    public Personne(int id, int age, String etat){
       this.idPersonne = id;
       this.age = age;
       this.etat = etat;
    }
    
     /**
     * Recupere l'id d'une personne.
     * @return un entier qu'est l'id
     */
    public int getIdPersonne(){
        return idPersonne;
    }
    
    /**
     * Recupere l'age d'une personne.
     * @return un entier qui est son age
     */
    public int getAge(){
        return age;
    }
    
    /**
     * Recupere l'etat d'une personne.
     * @return une chaine de caractere qui est son etat
     */
    public String getEtat(){
        return etat;
    }
    
    /**
     * Donne un etat à une personne.
     *@param etat une chaine de caractere
     */
    public void setEtat(String etat){
        this.etat = etat;
    }
    
}
 
