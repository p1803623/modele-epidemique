
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Classe qui represente un monde 2D de Personnes
 */
public class Monde {
    private int m,n;
    List<Personne>[][] monde;
    int nbPersonne;
    
    /**
     * Construit un monde de taille x, y avec nbPersonne placé aléatoirement
     * @param x taille de l'axe horizontale
     * @param y taille de l'axe verticale
     * @param nbpersonne nombre de personnes
     * 
     */
    public Monde(int x, int y,int nbpersonne){
        this.m = x;
        this.n = y;
        this.nbPersonne = nbpersonne;
        
        monde = new ArrayList[x][y];
        for(int j=0; j<x; j++){
            for(int k=0; k<y; k++){
                monde[j][k] = new ArrayList();
            }
        }
        
        for (int i=1 ;i<=nbpersonne;i++)
        {
           int randx;
           int randy;
           
           if(x==1){
               randx = 0;
           }
           else{
               randx = ThreadLocalRandom.current().nextInt(0, x-1);
           }
           
           if(y==1){
               randy = 0;
           }
           else{
               randy = ThreadLocalRandom.current().nextInt(0, y-1);
           }
          
           Personne p = new Personne(i,20, "S");
           monde[randx][randy].add(p);
           
        }
    }
    
     /**
     * Recupere la taille de l'axe horizontale du Monde.
     * @return un entier 
     */
    public int getM(){
        return m;
    }
    
     /**
     * Recupere la taille de l'axe verticale du Monde.
     * @return un entier 
     */
    public int getN(){
        return n;
    }
    
     /**
     * Recupere le nombre de personne d'un certain etat dans une case.
     * @param x entier designe l'axe horizontale
     * @param y entier designant l'axe verticale
     * @param etat chaine de caractere designant l'etat d'une personne
     * @return un entier 
     */
    public int getNbEtatDansCase(int x, int y, String etat){
        int nb = 0;
        if(monde[x][y] != null){
            for(int i=0; i<monde[x][y].size(); i++){
                if(monde[x][y].get(i).getEtat().equals(etat)){
                    nb++;
                }     
            }   
        }
  
        return nb;
    }
    
    /**
     * Recupere toute les personnes dans une case avec un certain etat
     * @param x entier designe l'axe horizontale
     * @param y entier designant l'axe verticale
     * @param etat chaine de caractere designant l'etat d'une personne
     * @return une liste de Personne 
     */
    public List<Personne> getPersonneDansCaseParEtat(int x, int y, String etat){
        List<Personne> listePersonne = new ArrayList();
        for(int i = 0; i<monde[x][y].size(); i++){
            if(etat.equals(monde[x][y].get(i).getEtat())){
                listePersonne.add(monde[x][y].get(i));
            }
        }
        
        return listePersonne;
    }
    
    
    /**
     * Procédure qui deplace toute les personnes du monde avec un pourcentage de chance de ce deplacer de chanceDeDeplacement pour chaque personne
     *@param  chanceDeDeplacement entier representant les chances d'une personne à se deplacer
     */
    public void deplacement(int chanceDeDeplacement){
        List<Personne>[][] nouveauMonde = new ArrayList[this.m][this.n];
        for(int x=0; x<this.m; x++){
            for(int y=0; y<this.n; y++){
                nouveauMonde[x][y] = new ArrayList();
            }
        }
        
        
        
        String choixDeplacement;
        Random rand = new Random(); 
        
        for(int i=0; i< this.m; i++){
            for(int j=0; j< this.n; j++){
                if(this.monde[i][j] != null){
                    for(int k=0; k<this.monde[i][j].size(); k++){
                        
                    ArrayList<String> possibility = new ArrayList<>(
                        Arrays.asList("gauche", "droite", "haut","bas"));
                    
                    // recupérer les choix possible pour un deplacement
                        if(i == 0){
                            possibility.remove("gauche");
                        }

                        if(i == m-1){
                            possibility.remove("droite");
                        }

                        if(j==0){
                            possibility.remove("haut");
                        }

                        if(j == n-1){
                            possibility.remove("bas");
                        }


                        // verifié si la personne fait un deplacement ou pas 
                        int randBetween1_100 = rand.nextInt(100)+1;
                        if(randBetween1_100 > chanceDeDeplacement){
                            choixDeplacement  = "";
                        }
                        else{
                             // choisir un choix de deplacement random 
                            if(possibility.isEmpty()){
                                choixDeplacement = "";
                            }
                            else{
                                int indexRandom = rand.nextInt(possibility.size());
                                choixDeplacement = possibility.get(indexRandom);
                            }
                        }

                        // efectuer le deplacement 
                        Personne personneEnDeplacement = this.monde[i][j].get(k);
                        switch(choixDeplacement){
                            case "gauche": nouveauMonde[i-1][j].add(personneEnDeplacement);
                                break;
                            case "droite": nouveauMonde[i+1][j].add(personneEnDeplacement);
                                break;
                            case "haut": nouveauMonde[i][j-1].add(personneEnDeplacement);
                                break;
                            case "bas": nouveauMonde[i][j+1].add(personneEnDeplacement);
                                break;
                            default: nouveauMonde[i][j].add(personneEnDeplacement);
                        }
                    }
                }
                
            }
        }
        
        // placer le nouveau monde comme le monde actuel
        this.monde = nouveauMonde;
        
    } 
    
    
    /**
     * Procedure qui assigne a toutes les personnes du monde un etat en fonction d'une liste d'etats.
     * @param listeEtats liste des etats a assigner
     */
    public void setEtats(List<String> listeEtats){
        int currentPersonneIndex = 0;
        for(int i = 0; i<m; i++){
            for(int j = 0; j<n; j++){
                for(int k=0; k<monde[i][j].size(); k++){
                    monde[i][j].get(k).setEtat(listeEtats.get(currentPersonneIndex));
                    currentPersonneIndex++;
                }
            }
        }
    }
    
    /**
     * Procedure qui change l'etat de nbPersonne avec comme etat etatDepart en etatArrive.
     * @param etatDepart une chaine de caractere designant l'etat actuel de la personne.
     * @param etatArrive une chaine de caractere designant l'etat d'arrive
     * @param nbPersonne nombre de personne a changer
     */
    public void ChangeEtat(String etatDepart,String etatArrive,int nbPersonne)
    {
        for(int i = 0 ; i<m ; i++)
        {
            for (int j = 0 ; j<n;j++)
            {
                for (int k = 0 ; k< monde[i][j].size(); k++)
                {
                    if(monde[i][j].get(k).getEtat().equals(etatDepart) && nbPersonne > 0)
                    {
                        monde[i][j].get(k).setEtat(etatArrive);
                        nbPersonne --;
                    }
                }
            }
        }
        
        
    }
    
     /**
     * Procedure qui retire nb personne avec un etat 'etat' de  la case x,y.
     * @param x entier axe horizontal
     * @param y entier axe vertical
     * @param nb entier designant le nombre de personne à retirer.
     * @param etat chaine de caractere 
     */
    public void removeXpersonneByEtatInCase(int x, int y, int nb, String etat){
        int i =0;
        while(nb > 0 && i<monde[x][y].size()){
            if(monde[x][y].get(i).getEtat().equals(etat)){
                monde[x][y].remove(i);
                this.nbPersonne--;
                nb--;
            }
            i++;
        }
    }
    
     /**
     * Procedure qui ajoute un nb personne dans la case x,y avec l'etat etat.
     * @param x entier axe horizontal
     * @param y entier axe vertical
     * @param nb entier designant le nombre de personne à ajouter.
     * @param etat chaine de caractere 
     */
    public void ajouteXpersonneWithEtatInCase(int x, int y, int nb, String etat){
        while(nb>0){
            Personne p = new Personne(nbPersonne+1, 1, etat);
            monde[x][y].add(p);
            nbPersonne++;
            nb--;
        }
    }
    
     /**
     * Procedure qui afffiche notre Monde.
     */
    public void afficherMonde(){
        System.out.println("Monde fin1");
        for(int i = 0; i<m ; i++){
            for(int j = 0; j<n ; j++){
                System.out.print("[");
                if(monde[i][j] == null){
                    System.out.print(0);
                }
                else{
                    System.out.print(monde[i][j].size());
                }
                System.out.print("],");
            }
            System.out.println("");
        }
        
        System.out.println("Monde fin1");
    }
}

