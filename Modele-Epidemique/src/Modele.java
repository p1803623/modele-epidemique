import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


/**
 * Classe abstraite qui definit un modèle de simulation épidemique
 */
abstract class Modele {
    protected int S,E,I,R;
    protected int dt;
    protected double Beta,Alpha,Gamma,Eta,Mu;
    protected Monde monde;
    protected PolitiquePublique politique;
    protected List<List<Integer>> tab;
    protected double coefPortDuMasque,coefVaccination,coefQuarantaine;
    
    
    /**
     * Procedure abstraite qui calcul les valeurs des differentes variables a un instant t.
     */
    abstract void calculer();
    
    abstract void affichageResultat();
    
    /**
     * Constructeur par defaut de la classe.
     * Le constructeur lit les valeurs saisies par l'utilisateur pour chacunes des variables.
     *@param  nomFichier nom du fichier à ouvrir pour lire les données.
     */
    public Modele(String nomFichier)
    {
        try
        {
            // le fichier d'entree
            File fichier = new File(nomFichier);
            // l'objet file reader
            FileReader read = new FileReader(fichier);
            // Creation de l'objet BufferedReader
            BufferedReader bf = new BufferedReader(read);
            String line;
            
            // Valeurs saisies par l'utilisateur
            line = bf.readLine();
            
            // On recupere la valeur de S
            line = bf.readLine();
            this.S = this.getIntValueInLine(line);
            
            // On recupere la valeur de E
            line = bf.readLine();
            this.E = this.getIntValueInLine(line);
            
            // On recupere la valeur de I
            line = bf.readLine();
            this.I = this.getIntValueInLine(line);
            
            
            // On recupere la valeur de R
            line = bf.readLine();
            this.R = this.getIntValueInLine(line);
            
            
            //Coef
            line = bf.readLine();
            line = bf.readLine();
            // On recupère la valeur de dAlpha
            line = bf.readLine();
            this.Alpha = this.getDoubleValueInLine(line);
            
            // On recupère la valeur de Beta
            line = bf.readLine();
            this.Beta = this.getDoubleValueInLine(line);
            
            // On recupère la valeur de Eta
            line = bf.readLine();
            this.Eta = this.getDoubleValueInLine(line);
            
            // On recupère la valeur de Gamma
            line = bf.readLine();
            this.Gamma = this.getDoubleValueInLine(line);
            
            // On recupère la valeur de Mu
            line = bf.readLine();
            this.Mu = this.getDoubleValueInLine(line);
            
            
            //Monde
            line = bf.readLine();
            line = bf.readLine();
            // On recupère la valeur de m
            line = bf.readLine();
            int m = this.getIntValueInLine(line);
            
            // On recupère la valeur de n
            line = bf.readLine();
            int n = this.getIntValueInLine(line);
            
            //Politique Publique
            line = bf.readLine();
            line = bf.readLine();
            // On recupère la valeur du confinement
            line = bf.readLine();
            boolean confinement = this.getBoolValueInLine(line);
            
            // On recupère la proportion de portDuMasque
            line = bf.readLine();
            boolean portDuMasque = this.getBoolValueInLine(line);
            
            // On recupère la proportion de quarantaine
            line = bf.readLine();
            boolean quarantaine = this.getBoolValueInLine(line);
            
             // On recupère la proportion de vaccination
            line = bf.readLine();
            boolean vaccination = this.getBoolValueInLine(line);
            
            line = bf.readLine();
            line = bf.readLine();
            this.coefPortDuMasque = this.getDoubleValueInLine(line);
            line = bf.readLine();
            this.coefVaccination = this.getDoubleValueInLine(line);
            line = bf.readLine();
            this.coefQuarantaine = this.getDoubleValueInLine(line);
                    
            read.close();
            this.politique = new PolitiquePublique(confinement,portDuMasque,quarantaine,vaccination);
            
            int nbPersonne = 0 ;
            if(this instanceof SEIR)
            {
                nbPersonne = S + E + I + R;
            }else{
                nbPersonne = S + I + R;
            }

            this.monde = new Monde(m,n,nbPersonne); 
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }  
    }
    
    /**
     * Procédure finale qui lance la simulation
     *@param  t entier qui correspond au nombres de pas de la simulation
     */
    final void simulation(int t){ 
        int coefDeplacement = 100;
        if (politique.getConfinement() == true)
        { 
                coefDeplacement = coefDeplacement/3;
        }

        for(int i=0; i<t; i++){
            // pour chaque pas de temps
            if (politique.getVaccination() == true)
            {
                int nbSain = tab.get(0).get(i);
                int nbVaccine =  (int) Math.round(nbSain*coefVaccination);
                monde.ChangeEtat("S","R",nbVaccine);
            }
            
            calculer();
            monde.deplacement(coefDeplacement);
           
        }
    }
    
    
     /**
     * Recupere la valeur voulue dans une ligne sous forme entier
     * @param line chaine de caractere de la ligne
     * @return l'entier voulue de la ligne passé en paramètre 
     */
    private int getIntValueInLine(String line)
    {
        String mots[]= line.split(" ");
        return Integer.parseInt(mots[2]);
    }
    
    /**
     * Recupere la valeur voulue dans une ligne sous forme de double
     * @param line chaine de caractere de la ligne
     * @return double voulue de la ligne passé en paramètre
     */
     private double getDoubleValueInLine(String line)
    {
        String mots []= line.split(" ");
        return Double.parseDouble(mots[2]);
    }
    
    /**
    * Recupere la valeur voulue dans une ligne sous forme de booleen
    * @param line chaine de caractere de la ligne
    * @return booleen voulue de la ligne passé en paramètre
    */
     private boolean getBoolValueInLine(String line)
    {
        String mots []= line.split(" ");
        return Boolean.parseBoolean(mots[2]);
    }

     /**
    * Procedure qui test si nos valeurs et coefficients ont correctement été saisies par l'utilisateur
    * (Affiche ces valeurs)
    */
     public void testConstructeur()
     {
        System.out.println("Valeur du nombre d'individu sain extraite du fichier :");
        System.out.println(this.S);
        System.out.println("Valeur du nombre d'individu exposé extraite du fichier :");
        System.out.println(this.E);
        System.out.println("Valeur du nombre d'individu infecté extraite du fichier :");
        System.out.println(this.I);
        System.out.println("Valeur du nombre d'individu retiré extraite du fichier :");
        System.out.println(this.R);
        System.out.println("Probabilité d'etre infecté en etant exposé extraite du fichier :");
        System.out.println(this.Alpha);
        System.out.println("Probabilité d'infection en etant sain extraite du fichier :");
        System.out.println(this.Beta);
        System.out.println("Proportion de naissance extraite du fichier :");
        System.out.println(this.Eta);
        System.out.println("Probabilité d'etre retiré en etant infecté extraite du fichier :");
        System.out.println(this.Gamma);
        System.out.println("Proportion de mort naturelle extraite du fichier :");
        System.out.println(this.Mu);
        System.out.println("Proportion d'individu portant le Masque :");
        System.out.println(this.coefPortDuMasque);
        System.out.println("Proportion d'individu vacciné : ");
        System.out.println(this.coefVaccination);
        System.out.println("Proportion d'individu en quarantaine :");
        System.out.println(this.coefQuarantaine);
        
     }
          

}
