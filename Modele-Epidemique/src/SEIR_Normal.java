
import java.util.concurrent.ThreadLocalRandom;



/**
 * Classe SEIR qui represente le modèle SEIR Normal.
 */
public class SEIR_Normal extends SEIR{
    /**
     * Constructeur de la classe.
     * Appel le constructeur de SEIR
     *@param  nomFichier nom du fichier à ouvrir pour lire les données.
     */
    public SEIR_Normal(String nomFichier){
        super(nomFichier);
    }
     
   /**
    * Classe abstraite permettant de recuperer le nombre de personnes passant d'un etat à un autre.
    * @param S entier representant le nombre de sain
    * @param E entier representant le nombre d'exposé
    * @param I entier representant le nombre d'infecté
    * @param R entier representant le nombre de retiré
    * @param x taille de l'axe horizontale
    * @param y taille de l'axe verticale
    * @return tableau des transitions ([0]: S to E, [1]: E to I, [2]: I to R)
    */
    @Override
    protected int[] getTransitions(int S, int E, int I, int R, int x, int y){
        
        // Gestion PortDuMasque
        double BetaPortDuMasque = Beta ;
        if (politique.getPortDuMasque() == true )
            {
                int randBetween1_100 = ThreadLocalRandom.current().nextInt(1, 100);
                if(randBetween1_100 <= coefPortDuMasque * 100)
                {
                    BetaPortDuMasque = Beta*0.5;
                }
            }
        
        //Gestion Quarantaine
        int quarantaine = 1;
        if (politique.getQuarantaine() == true)
        {
            int randBetween1_100 = ThreadLocalRandom.current().nextInt(1, 100);
            if(randBetween1_100 <= coefQuarantaine * 100)
            {
                quarantaine = 0;
            }
        }
        
        int[] transitions = new int[3];
        
        
        transitions[0] = getNbPersonneGoStoE(S, I, BetaPortDuMasque,quarantaine);
        transitions[1] = getNbPersonneGoEtoI(E);
        transitions[2] = getNbPersonneGoItoR(I);
        
        return transitions;
    }
    
     /**
     * Retourne le nombre de personne passant de S à E.
     * @param S entier representant le nombre de sain
     * @param I entier representant le nombre d'infecté
     * @param beta proportion de personne infectée
     * @param quarantaine proportion de personne en quarantaine
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoStoE(int S, int I,double beta,int quarantaine){
        return (int) Math.round(beta*S*I*quarantaine); 
    }
    
    /**
     * Retourne le nombre de personne passant de E à I.
     * @param E entier representant le nombre d'exposé
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoEtoI(int E){
        return (int) Math.round(Alpha*E) ;
    }
    
    /**
     * Retourne le nombre de personne passant de I à R.
     * @param I entier representant le nombre d'infecté
     * @return un entier qui correspond à ce nombre
     */
    private int getNbPersonneGoItoR(int I){
        return (int) Math.round(Gamma*I);
    }
}
